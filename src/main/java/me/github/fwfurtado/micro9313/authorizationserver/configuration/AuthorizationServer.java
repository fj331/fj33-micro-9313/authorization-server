package me.github.fwfurtado.micro9313.authorizationserver.configuration;

import me.github.fwfurtado.micro9313.authorizationserver.login.LoginService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

    private final String clientSecret;
    private final AuthenticationManager authenticationManager;
    private final LoginService loginService;

    public AuthorizationServer(@Value("${app.defaultPassword}") String clientSecret, AuthenticationManager authenticationManager, LoginService loginService) {
        this.clientSecret = clientSecret;
        this.authenticationManager = authenticationManager;
        this.loginService = loginService;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
            .inMemory()
                .withClient("frontend")
                .secret(clientSecret)
                .authorizedGrantTypes("password", "refresh_token")
                .scopes("write:user");
    }


    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()");
    }


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .accessTokenConverter(converter())
                .tokenStore(tokenStore())
                .userDetailsService(loginService)
                .authenticationManager(authenticationManager);
    }

    @Bean
    JwtAccessTokenConverter converter() {
        var jwtAccessTokenConverter = new JwtAccessTokenConverter();

        jwtAccessTokenConverter.setSigningKey("signkey");

        return jwtAccessTokenConverter;
    }

    @Bean
    JwtTokenStore tokenStore() {
        return new JwtTokenStore(converter());
    }

}
