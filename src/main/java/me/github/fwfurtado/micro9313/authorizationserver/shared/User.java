package me.github.fwfurtado.micro9313.authorizationserver.shared;

import java.util.Set;

public class User {
    private Long id;
    private String email;
    private String password;
    private String[] permissions;

    public User(Long id, String email, String password, String[] permissions) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.permissions = permissions;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String[] getPermissions() {
        return permissions;
    }
}
