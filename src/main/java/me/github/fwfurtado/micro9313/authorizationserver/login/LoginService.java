package me.github.fwfurtado.micro9313.authorizationserver.login;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements UserDetailsService {
    private final LoginRepository repository;
    private final ResourceOwnerMapper mapper;

    public LoginService(LoginRepository repository, ResourceOwnerMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByEmail(username).map(mapper::map)
                .orElseThrow(() -> new UsernameNotFoundException("Cannot find user!"));
    }
}
