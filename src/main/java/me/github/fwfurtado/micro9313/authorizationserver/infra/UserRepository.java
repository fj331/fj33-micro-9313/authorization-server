package me.github.fwfurtado.micro9313.authorizationserver.infra;

import me.github.fwfurtado.micro9313.authorizationserver.login.LoginRepository;
import me.github.fwfurtado.micro9313.authorizationserver.shared.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepository implements LoginRepository {

    private final String defaultPassword;

    public UserRepository(@Value("${app.defaultPassword}") String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.of(new User(1L, "fernando.furtado@caelum.com.br", defaultPassword, new String[]{"customer"}));
    }
}
