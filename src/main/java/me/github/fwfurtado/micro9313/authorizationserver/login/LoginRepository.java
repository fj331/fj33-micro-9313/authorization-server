package me.github.fwfurtado.micro9313.authorizationserver.login;


import me.github.fwfurtado.micro9313.authorizationserver.shared.User;

import java.util.Optional;

public interface LoginRepository {

    Optional<User> findByEmail(String email);
}
