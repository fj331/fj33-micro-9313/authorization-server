package me.github.fwfurtado.micro9313.authorizationserver.login;

import me.github.fwfurtado.micro9313.authorizationserver.shared.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

@Component
public class ResourceOwnerMapper {

    public ResourceOwner map(User user) {
        var authorityList = AuthorityUtils.createAuthorityList(user.getPermissions());
        return new ResourceOwner(user.getEmail(), user.getPassword(), authorityList);
    }
}
